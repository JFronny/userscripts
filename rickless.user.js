// ==UserScript==
// @name        Rickless 
// @namespace   JFronny// @include *
// @grant       none
// @version     1.0
// @author      -
// @description 9.9.2021, 15:56:04
// @require  http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js
// @require  https://gist.github.com/raw/2625891/waitForKeyElements.js
// ==/UserScript==

const blocked_ids = [
  "dQw4w9WgXcQ",
  "-51AfyMqnpI",
  "oHg5SJYRHA0",
  "cvh0nX08nRw",
  "V-_O7nl0Ii0",
  "X9NOzYMLfmM",
  "6_b7RDuLwcI",
  "m2ATf01v4hw",
  "IC5YozmvPpM",
  "IAISUDbjXj0",
  "rZRb_JeBLus",
  "9NcPvmk4vfo",
  "VzuDnbjIhbg",
  "DLzxrzFCyOs",
  "dQw4w9WgXcT",
  "dQw4w9WgXcR",
  "TMjyJfqOlbI",
  "S65ykNd9D7c"
];

const id = "jfrickless";

function rewriteLinks(jNode) {
  var el = jNode.get(0);
  if (el.getAttribute(id) != null)
    return;
  el.setAttribute(id, "testing");
  var href = el.getAttribute("href");
  for (const u of blocked_ids) {
    if (href.indexOf(u) > -1) {
      el.innerHTML = el.innerHTML + '<p style="background-color: #333; color: #eee; font-weight: bold; font-size: 1.23em; font-style: italic; display: inline;" >Rickroll</p>';
      el.setAttribute(id, "found");
    }
    else {
      el.setAttribute(id, "safe");
    }
  }
}

waitForKeyElements("a", rewriteLinks);